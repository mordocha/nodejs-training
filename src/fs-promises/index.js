// 4/1/index.js
const path = require('path')

const fsPromises = require('./fs-promises'); //import the module you have written
fsPromises.readFile(path.resolve('src/fs-promises/alignment_751_genomic.fasta'))
    .then((content) => {
        console.log(content); // prints the content of the file
    })
    .catch((err) => {
        console.error(err); // prints the error
    });
