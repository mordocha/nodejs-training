const fs = require('fs');

const readFilePromies = (path) => {
    const promise = new Promise(function(resolve, reject) {
        fs.readFile(path, 'utf8', (error, data) => {
            if (error) {
                reject(error);
            }
            resolve(data);
        })
    });
    return promise;
}

module.exports = {
    readFile: readFilePromies
};