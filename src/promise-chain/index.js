const fs = require('fs');
const path = require('path');

const chain = new Promise((resolve) => {
    console.log('hello');
    setTimeout(() => resolve(), 3000);
});

const writeFilePromise = (path) => {
    const promise = new Promise(function(resolve, reject) {
        fs.writeFile(path, 'hello world', (error, data) => {
            if (error) {
                reject(error);
            }
            resolve(data);
        })
    });
    return promise;
}

chain.then(() => {
    console.log('world');
    return new Promise((resolve) => {
        setTimeout(() => resolve(), 2000);
    });
}).then(() => {
    return writeFilePromise(path.resolve('src/promise-chain/out.txt'))
}).then(() => console.log('done!'))