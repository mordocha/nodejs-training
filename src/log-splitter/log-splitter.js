const fs = require('fs');
const path = require('path');
const readline = require('readline');
const countLinesInFile = require('count-lines-in-file');

const getPath = (filename) => {
  return path.resolve(__dirname, filename);
}

const fileReader = (fileName, numberOfFiles) => {
  const [name, extension] = fileName.split('.');
  const targetFilePath = getPath(fileName);
  const parentFolderPath = path.dirname(targetFilePath);

  countLinesInFile(targetFilePath, (error, numberOfLines) => {
    const numberOfLinesPerFile = Math.ceil(numberOfLines / numberOfFiles);
    console.log(numberOfLinesPerFile);
    let fileIndex = 1;
    let counter = 0;
    const readInterface = readline.createInterface(fs.createReadStream(targetFilePath));
    readInterface.on('line', (line) => {
      if (counter === numberOfLinesPerFile) {
        fileIndex++;
        counter = 0;
      }
      fs.appendFile(path.join(parentFolderPath, `${name}-${fileIndex}.${extension}`), 
          line + '\n', () => {});
      counter++;
    });
    readInterface.on('close', () => {
      console.log('Done reading the long file!');
    })
  });
}


const inputFileName = process.argv[2];
const fileNum = parseInt(process.argv[3]);

fileReader(inputFileName, fileNum);