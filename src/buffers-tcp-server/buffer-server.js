const net = require('net');
const parseOpcodeAndCalc = require('./buffer-ops-calculator')

const server = net.createServer((socket) => {
    console.log('Buffer calculator server');
	socket.on('data', (data) => {
        const value = parseOpcodeAndCalc(data);
        socket.write(value.toString());
    })
  }).on('error', (err) => {
    throw err;
  });  
  
  server.listen({
      port: 8000,
      host: '127.0.0.1'
    }, () => {
    console.log('opened server on', server.address());
  });