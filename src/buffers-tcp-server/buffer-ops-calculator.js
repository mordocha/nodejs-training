const SUM_OPCODE = 0xA0;
const MUL_OPCODE = 0xA1;
const AVG_OPCODE = 0xA2;

const FIRST_OPERAND = 1;
const SECOND_OPERAND = 3;
const BIG_INT_BYTES_SIZE = 2;

const OPCODE_DICT = {
    0xA0: (buffer) => {return sum(buffer)},
    0xA1: (buffer) => {return mul(buffer)},
    0xA2: (buffer) => {return avg(buffer)}
}

const sum = (buffer) => {
    return buffer.readInt16BE(FIRST_OPERAND) + buffer.readInt16BE(SECOND_OPERAND);
}

const mul = (buffer) => {
    return buffer.readInt16BE(FIRST_OPERAND) * buffer.readInt16BE(SECOND_OPERAND);
}

const avg = (buffer) => {
    const numValues = buffer.readUInt8(1);
    const values = [];
    for (let index = 0; index < numValues * BIG_INT_BYTES_SIZE; index += BIG_INT_BYTES_SIZE) {
        values.push(buffer.readInt16BE(index + BIG_INT_BYTES_SIZE));   
    }
    console.log(values.toString())
    return values.reduce((a,b) => {return a + b}) / numValues;
}

function parseOpcodeAndCalc(buffer) {
    const opcode = buffer.readUInt8(0);
    return OPCODE_DICT[opcode](buffer);
}

module.exports =  parseOpcodeAndCalc;