const net = require('net');

const client = new net.Socket();
client.connect(8000, '127.0.0.1', function() {
	console.log('Connected');
	client.write(Buffer.from([0xA2, 0x03, 0x00, 0x06, 0x00, 0x08, 0x00, 0x09]));
});

client.on('data', function(data) {
	console.log('Received: ' + data);
	client.destroy(); 
});

client.on('close', function() {
	console.log('Connection closed');
});
