const net = require('net');

const server = net.createServer((socket) => {
    console.log('Echo server');
	socket.pipe(socket);
  }).on('error', (err) => {
    // Handle errors here.
    throw err;
  });  
  

  server.listen({
      port: 8000,
      host: '127.0.0.1'
    }, () => {
    console.log('opened server on', server.address());
  });