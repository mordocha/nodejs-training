const net = require('net');

const connectedSockets = new Set();

const server = net.createServer((socket) => {
    console.log('Echo server');
    connectedSockets.add(socket);

    socket.on('end', () => {
        connectedSockets.delete(socket);
    });

    socket.on('data', (data) => {
        for (let sock of connectedSockets) {
            if (sock !== socket) {
                sock.write(data);
            }
        }
    })
  }).on('error', (err) => {
    // Handle errors here.
    throw err;
  });  
  

  server.listen({
      port: 8000,
      host: '127.0.0.1'
    }, () => {
    console.log('opened server on', server.address());
  });